<?php

namespace Del\Simcode;

class Handler
{
    const MODULE_ID = 'del.simcode';

    public static function CheckData(&$arArgs)
    {
        $arOptions = \Bitrix\Main\Config\Option::getForModule(self::MODULE_ID);
        $flag=0;
        $arIblock = explode(',', $arOptions['listIblockName']);

        if (in_array($arArgs['IBLOCK_ID'], $arIblock)) {
            global $USER;

            $arTransParams = array( //(максимальная длина кода,регистр(нижний,верхний,не менять),
                //заменять пробел на...,остальные символы менять на...,удалять лишние символы замены)
                "max_len" => $arOptions['maxLengthTranslit'],
                "change_case" => $arOptions['registerValue'], // 'L' - toLower, 'U' - toUpper, false - do not change
                "replace_space" => $arOptions['changeSimbolToSpace'],
                "replace_other" => $arOptions['changeSimbolToOther'],
                "delete_repeat_replace" => true
            );

            $codeName = \CUtil::translit($arArgs['NAME'], "ru", $arTransParams);
            // AddMessage2Log($codeName, "block3");

            $el = new \CIBlockElement;
            $arProps = ["MODIFIED_BY" => $USER->GetID(), "CODE" => $codeName];
            $res = $el->Update($arArgs['ID'], $arProps);

        }

        return $flag;
    }
}
