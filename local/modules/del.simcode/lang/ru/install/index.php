<?
$MESS['MYNAME_MODULE_NAME'] = 'Заготовка модуля для дальнейшей работы';
$MESS['MYNAME_MODULE_MODULE_DESCRIPTION'] = 'Заготовка модуля, которая позволяет создавать легко и просто модули 1С-Битрикс';
$MESS['MYNAME_MODULE_PARTNER_NAME'] = 'DelCompany';
$MESS["MYNAME_MODULE_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста, обновите систему.";
$MESS["MYNAME_MODULE_PARTNER_URI"] = "https://vk.com/morya4ok10";