<?

/**
 * CControllerClient::GetInstalledOptions($module_id);
 * формат массива, элементы:
 * 1) ID опции (id инпута)(Берется с помощью COption::GetOptionString($module_id, $Option[0], $Option[2]) если есть)
 * 2) Отображаемое имя опции
 * 3) Значение по умолчанию (так же берется если первый элемент равен пустой строке), зависит от типа:
 *      checkbox - Y если выбран
 *      text/password - htmlspecialcharsbx($val)
 *      selectbox - одно из значений, указанных в массиве опций
 *      multiselectbox - значения через запятую, указанные в массиве опций
 * 4) Тип поля (массив)
 *      1) Тип (multiselectbox, textarea, statictext, statichtml, checkbox, text, password, selectbox)
 *      2) Зависит от типа:
 *         text/password - атрибут size
 *         textarea - атрибут rows
 *         selectbox/multiselectbox - массив опций формата ["Значение"=>"Название"]
 *      3) Зависит от типа:
 *         checkbox - доп атрибут для input (просто вставляется строкой в атрибуты input)
 *         textarea - атрибут cols
 *
 *      noautocomplete) для text/password, если true то атрибут autocomplete="new-password"
 *
 * 5) Disabled = 'Y' || 'N';
 * 6) $sup_text - ??? текст маленького красного примечания над названием опции
 * 7) $isChoiceSites - Нужно ли выбрать сайт??? флаг Y или N
 */


use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

\Bitrix\Main\Loader::includeModule('iblock');

Loc::loadMessages(__FILE__);

$request = HttpApplication::getInstance()->getContext()->getRequest();

$module_id = htmlspecialcharsbx($request["mid"] != "" ? $request["mid"] : $request["id"]);

//For paid modules only BEGIN
$isExpired = CModule::IncludeModuleEx($module_id);
if ($isExpired != 3) {
//For paid modules only END  
    $error = [];
    $arSelect = [];
    $dbItemsIblock = \Bitrix\Iblock\IblockTable::getList(array(
        'order' => array('SORT' => 'ASC'), // сортировка
        'select' => array('ID', 'NAME'), // выбираемые поля, без свойств. Свойства можно получать на старом ядре \CIBlockElement::getProperty
        'filter' => array('ACTIVE' => 'Y'), // фильтр только по полям элемента, свойства (PROPERTY) использовать нельзя
        'group' => array(), // группировка по полю, order должен быть пустой
        'limit' => 1000, // целое число, ограничение выбираемого кол-ва
        'offset' => 0, // целое число, указывающее номер первого столбца в результате
        'count_total' => 1, // дает возможность получить кол-во элементов через метод getCount()
        'runtime' => array(), // массив полей сущности, создающихся динамически
        'data_doubling' => false, // разрешает получение нескольких одинаковых записей
    ));

    while($itemIblock = $dbItemsIblock->fetch()) {
        $arSelect[$itemIblock['ID']] = $itemIblock['NAME'];

    }

    $arSelectRegister = [
        "false" => Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_SELECT_REGISTER_DEFAULT"),
        "L" => Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_SELECT_REGISTER_LOWER"),
        "U" => Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_SELECT_REGISTER_UPPER")
    ];

    $aTabs = array(
    	array(
    		"DIV" 	  => "edit",
    		"TAB" 	  => Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_COMMON"),
    		"TITLE"   => Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_NAME"),
    		"OPTIONS" => array(
    			Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_COMMON"),
                array(
    				"listIblockName",
    				Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_MULTISELECT"),
    				'NONE',
    				array("multiselectbox",$arSelect)
    			),
                array(
    				"maxLengthTranslit",
    				Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_MAX_LENGTH_TRANSLIT"),
    				'100',
    				array("text",5)
    			),
                array(
    				"registerValue",
    				Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_SELECT_REGISTER"),
    				'NONE',
    				array("selectbox",$arSelectRegister)
    			),
                array(
    				"changeSimbolToSpace",
    				Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_CHANGE_SPACE"),
    				'-',
    				array("text",2)
    			),
                array(
    				"changeSimbolToOther",
    				Loc::getMessage("MYNAME_MODULE_OPTIONS_TAB_CHANGE_OTHER"),
    				'-',
    				array("text",2)
    			), 
    		)
    	)
    );
    
    if($request->isPost() && check_bitrix_sessid()){
    
    	foreach($aTabs as $aTab){
    
    		foreach($aTab["OPTIONS"] as $arOption){
    
    			if(!is_array($arOption)){
    
    				continue;
    			}
    
    			if($arOption["note"]){
    
    				continue;
    			}
    
    			if($request["apply"]){
    
    				$optionValue = $request->getPost($arOption[0]);

                    if ($arOption[0] == "maxLengthTranslit") {
                        if (!(int)$optionValue) $error["maxLengthTranslit"] = "Значение должно быть целым числом";
                    }
                    
                    if (!$error)
    				    Option::set($module_id, $arOption[0], is_array($optionValue) ? implode(",", $optionValue) : $optionValue);
    			} elseif($request["default"]){
    
    				// Option::set($module_id, $arOption[0], $arOption[2]);
    			}
    		}
    	}
    
    	LocalRedirect($APPLICATION->GetCurPage()."?mid=".$module_id."&lang=".LANG);
    }
    
    
    $tabControl = new CAdminTabControl(
    	"tabControl",
    	$aTabs
    );
    
    $tabControl->Begin();
    ?>
    <form action="<? echo($APPLICATION->GetCurPage()); ?>?mid=<? echo($module_id); ?>&lang=<? echo(LANG); ?>" method="post" id="setparams">
    
    	<?
    	foreach($aTabs as $aTab){
    
    		if($aTab["OPTIONS"]){
    
    			$tabControl->BeginNextTab();
    
    			__AdmSettingsDrawList($module_id, $aTab["OPTIONS"]);
    		}
    	}
    
    	$tabControl->Buttons();
    	?>
    	<input type="submit" name="apply" value="<? echo(Loc::GetMessage("MYNAME_MODULE_OPTIONS_APPLY")); ?>" class="adm-btn-save" />
    	<?
    	echo(bitrix_sessid_post());
    	?>
    
    </form>
    <?
    $tabControl->End();
    ?>
<? //For paid modules only BEGIN ?>   
<? } else {
        echo Loc::GetMessage("MYNAME_MODULE_EXPIRED");
    }
?>    
<? //For paid modules only END ?>